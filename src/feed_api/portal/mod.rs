mod error;

pub use self::error::{PortalError, PortalErrorKind};
use crate::models::{Article, ArticleID, Category, CategoryID, Feed, FeedID, FeedMapping, Headline, Tag};

pub type PortalResult<T> = Result<T, PortalError>;

pub trait Portal: Sync + Send {
    fn get_headlines(&self, ids: &[ArticleID]) -> PortalResult<Vec<Headline>>;
    fn get_articles(&self, ids: &[ArticleID]) -> PortalResult<Vec<Article>>;
    fn get_article_exists(&self, id: &ArticleID) -> PortalResult<bool>;
    fn get_article_ids_unread_feed(&self, feed_id: &FeedID) -> PortalResult<Vec<ArticleID>>;
    fn get_article_ids_unread_category(&self, category_id: &CategoryID) -> PortalResult<Vec<ArticleID>>;
    fn get_article_ids_unread_all(&self) -> PortalResult<Vec<ArticleID>>;
    fn get_article_ids_marked_all(&self) -> PortalResult<Vec<ArticleID>>;
    fn get_feeds(&self) -> PortalResult<Vec<Feed>>;
    fn get_categories(&self) -> PortalResult<Vec<Category>>;
    fn get_mappings(&self) -> PortalResult<Vec<FeedMapping>>;
    fn get_tags(&self) -> PortalResult<Vec<Tag>>;
}
