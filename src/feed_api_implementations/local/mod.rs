pub mod metadata;

use self::metadata::LocalMetadata;
use crate::models::{
    self, ArticleID, Category, CategoryID, FatArticle, FavIcon, Feed, FeedID, LoginData, Marked, PluginCapabilities, Read, SyncResult, TagID, Url,
};
use crate::util::html2text::Html2Text;
use crate::util::text2html::Text2Html;
use crate::{
    feed_api::{FeedApi, FeedApiErrorKind, FeedApiResult, Portal},
    models::Enclosure,
};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use failure::ResultExt;
use feed_rs::{
    model::{Entry, Link, MediaContent, MediaObject},
    parser,
};
use log::{error, warn};
use parking_lot::RwLock;
use rayon::prelude::*;
use reqwest::Client;
use std::hash::Hasher;
use std::str;
use std::{collections::hash_map::DefaultHasher, sync::Arc};

pub struct LocalRSS {
    portal: Box<dyn Portal>,
}

impl LocalRSS {
    fn select_article_url(links: &Vec<Link>) -> Option<Url> {
        let mut url = links
            .iter()
            .find(|l| l.rel.as_deref() == Some("alternate"))
            .map(|l| Url::parse(&l.href).ok())
            .flatten();

        if url.is_none() {
            for link in links {
                if let Ok(parsed_url) = Url::parse(&link.href) {
                    url = Some(parsed_url);
                    break;
                }
            }
        }

        url
    }
}

#[async_trait]
impl FeedApi for LocalRSS {
    fn features(&self) -> FeedApiResult<PluginCapabilities> {
        Ok(PluginCapabilities::ADD_REMOVE_FEEDS
            | PluginCapabilities::SUPPORT_CATEGORIES
            | PluginCapabilities::MODIFY_CATEGORIES
            | PluginCapabilities::SUPPORT_TAGS)
    }

    fn has_user_configured(&self) -> FeedApiResult<bool> {
        Ok(true)
    }

    async fn is_logged_in(&self, _client: &Client) -> FeedApiResult<bool> {
        Ok(true)
    }

    fn user_name(&self) -> Option<String> {
        None
    }

    fn get_login_data(&self) -> Option<LoginData> {
        Some(LoginData::None(LocalMetadata::get_id()))
    }

    async fn login(&mut self, _data: LoginData, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn logout(&mut self, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn initial_sync(&self, client: &Client) -> FeedApiResult<SyncResult> {
        let feeds = self.portal.get_feeds().context(FeedApiErrorKind::Portal)?;
        let mut future_store = Vec::new();

        for feed in feeds {
            let client = client.clone();
            future_store.push(async move {
                let mut articles: Vec<FatArticle> = Vec::new();
                let enclosures: Arc<RwLock<Vec<Enclosure>>> = Arc::new(RwLock::new(Vec::new()));

                if let Some(url) = &feed.feed_url {
                    let feed_request = match client.get(url.as_str()).send().await {
                        Ok(response) => response,
                        Err(error) => {
                            error!("Downloading feed failed: {} - {}", url, error);
                            return (Vec::new(), Vec::new());
                        }
                    };

                    if !feed_request.status().is_success() {
                        error!("Downloading feed failed: {}", url);
                        return (Vec::new(), Vec::new());
                    }

                    let content_location = feed_request
                        .headers()
                        .get(reqwest::header::CONTENT_LOCATION)
                        .map(|header_val| header_val.clone());

                    let result_bytes = match feed_request.bytes().await {
                        Ok(result_bytes) => result_bytes,
                        Err(_) => {
                            error!("Reading response as string failed: {}", url);
                            return (Vec::new(), Vec::new());
                        }
                    };

                    let feed_base_url = if let Some(content_location) = &content_location {
                        let content_location = content_location.to_str().ok();
                        content_location.map(|cl| Url::parse(cl).ok()).flatten()
                    } else {
                        feed.feed_url.as_ref().map(|url| url.base())
                    };
                    let feed_base_url = feed_base_url.map(|uri| uri.to_string());

                    let parse_result = parser::parse_with_uri(result_bytes.as_ref(), feed_base_url.as_deref());

                    if let Ok(parsed_feed) = parse_result {
                        let enclosures_ref = enclosures.clone();
                        let mut feed_articles = parsed_feed
                            .entries
                            .into_par_iter()
                            .filter_map(move |entry| {
                                let Entry {
                                    id,
                                    updated,
                                    title,
                                    authors,
                                    content,
                                    links,
                                    summary: entry_summary,
                                    categories: _,
                                    contributors: _,
                                    published,
                                    source: _,
                                    rights: _,
                                    media,
                                } = entry;

                                let article_id = ArticleID::new(&id);
                                let article_url = Self::select_article_url(&links);

                                let local_article = self
                                    .portal
                                    .get_articles(&vec![article_id.clone()])
                                    .ok()
                                    .map(|v| v.first().cloned())
                                    .flatten();

                                let mut marked = Marked::Unmarked;
                                let mut unread = Read::Unread;

                                // if article exists in db and already has same timestamp as 'updated' then skip it
                                if let Some(local_article) = local_article {
                                    if let Some(updated) = entry.updated {
                                        if local_article.date >= updated.naive_utc() {
                                            return None;
                                        }
                                    } else {
                                        // we have no updated timestamp for this article and it already exists in the db
                                        // so we skip this one
                                        return None;
                                    }

                                    marked = local_article.marked;
                                    unread = local_article.unread;
                                }

                                // FIXME: handle content-type
                                let xml_base = content.as_ref().map(|c| c.src.as_ref().map(|l| l.href.clone())).flatten();
                                let html = match content.map(|c| c.body).flatten() {
                                    Some(html) => {
                                        if let Some(Ok(xml_base)) = xml_base.map(|xb| Url::parse(&xb)) {
                                            if let Some(html_fixed_urls) = crate::util::complete_urls(&html, &xml_base) {
                                                Some(html_fixed_urls)
                                            } else {
                                                Some(html)
                                            }
                                        } else {
                                            Some(html)
                                        }
                                    }
                                    None => media.first().map(|m| (&m.description).as_ref()).flatten().map(|t| t.content.clone()),
                                };
                                let plain_text = match &entry_summary {
                                    Some(summary) => match Html2Text::process(&summary.content) {
                                        Some(summary) => Some(summary),
                                        None => {
                                            let summary = match escaper::decode_html(&summary.content) {
                                                Ok(summary) => summary,
                                                Err(e) => {
                                                    warn!("Error {:?} at character {}", e.kind, e.position);
                                                    summary.content.clone()
                                                }
                                            };
                                            let summary = str::replace(&summary, "\n", " ");
                                            let summary = str::replace(&summary, "\r", " ");
                                            let summary = str::replace(&summary, "_", " ");
                                            Some(summary)
                                        }
                                    },
                                    None => match &html {
                                        Some(html) => Html2Text::process(html),
                                        None => None,
                                    },
                                };
                                let summary = plain_text.as_deref().map(|t| Html2Text::to_summary(t));
                                let html = match html {
                                    Some(html) => Some(html),
                                    None => match &entry_summary {
                                        Some(original_summary) => Some(original_summary.content.clone()),
                                        None => plain_text.clone(),
                                    },
                                };

                                let html = html.map(|s| if !Text2Html::is_html(&s) { Text2Html::process(&s) } else { s });

                                let thumbnail_url = media
                                    .iter()
                                    .filter_map(|media| {
                                        let MediaObject { content, thumbnails, .. } = media;
                                        let attached_images: Vec<Url> = content
                                            .iter()
                                            .map(|content| {
                                                let MediaContent { url, content_type, .. } = content;
                                                content_type
                                                    .as_ref()
                                                    .map(|mime| {
                                                        if mime.type_() == "image" {
                                                            if let Some(url) = url {
                                                                Some(Url::new(url.clone()))
                                                            } else {
                                                                None
                                                            }
                                                        } else {
                                                            None
                                                        }
                                                    })
                                                    .flatten()
                                            })
                                            .flatten()
                                            .collect();
                                        let thumbnails = thumbnails.iter().map(|t| t.image.uri.as_str()).collect::<Vec<&str>>();
                                        if let Some(&thumbnail) = thumbnails.first() {
                                            Some(thumbnail.to_owned())
                                        } else if let Some(first_image) = attached_images.first() {
                                            Some(first_image.to_string())
                                        } else {
                                            None
                                        }
                                    })
                                    .collect::<Vec<String>>()
                                    .first()
                                    .cloned();

                                let mut entry_enclosures = media
                                    .iter()
                                    .flat_map(|media| {
                                        let MediaObject { content, .. } = media;
                                        content
                                            .iter()
                                            .map(|content| {
                                                let MediaContent { url, content_type, .. } = content;
                                                url.as_ref().map(|url| {
                                                    let url = Url::new(url.clone());

                                                    Enclosure {
                                                        article_id: article_id.clone(),
                                                        url,
                                                        mime_type: content_type.as_ref().map(|mime| mime.to_string()),
                                                        title: None,
                                                    }
                                                })
                                            })
                                            .flatten()
                                            .collect::<Vec<Enclosure>>()
                                    })
                                    .collect::<Vec<Enclosure>>();
                                enclosures_ref.write().append(&mut entry_enclosures);

                                let article = FatArticle {
                                    article_id,
                                    feed_id: feed.feed_id.clone(),
                                    title: title
                                        .map(|t| match escaper::decode_html(&t.content) {
                                            Ok(title) => Some(title),
                                            Err(_error) => {
                                                // This warning freaks users out for some reason
                                                // warn!("Error {:?} at character {}", error.kind, error.position);
                                                Some(t.content)
                                            }
                                        })
                                        .flatten(),
                                    url: article_url,
                                    author: authors.get(0).map(|person| person.name.clone()),
                                    date: match published {
                                        Some(published) => published,
                                        None => match updated {
                                            Some(updated) => updated,
                                            None => Utc::now(),
                                        },
                                    }
                                    .naive_utc(),
                                    synced: Utc::now().naive_utc(),
                                    direction: None,
                                    marked,
                                    unread,
                                    html,
                                    scraped_content: None,
                                    summary,
                                    plain_text,
                                    thumbnail_url,
                                };

                                Some(article)
                            })
                            .collect();
                        articles.append(&mut feed_articles);
                    } else {
                        error!("Couldn't parse feed content");
                    }
                } else {
                    warn!("No feed url for feed: '{}'", feed.feed_id);
                }

                return (articles, (*enclosures.read()).clone());
            });
        }

        let mut articles: Vec<FatArticle> = Vec::new();
        let mut enclosures: Vec<Enclosure> = Vec::new();

        let mut result_vec = futures::future::join_all(future_store).await;
        let _ = result_vec
            .drain(..)
            .map(|(mut feed_articles, mut feed_enclosures)| {
                articles.append(&mut feed_articles);
                enclosures.append(&mut feed_enclosures);
            })
            .collect::<()>();

        Ok(SyncResult {
            feeds: None,
            categories: None,
            mappings: None,
            tags: None,
            headlines: None,
            articles: if articles.is_empty() { None } else { Some(articles) },
            enclosures: if enclosures.is_empty() { None } else { Some(enclosures) },
            taggings: None,
        })
    }

    async fn sync(&self, _max_count: u32, _last_sync: DateTime<Utc>, client: &Client) -> FeedApiResult<SyncResult> {
        self.initial_sync(client).await
    }

    async fn set_article_read(&self, _articles: &[ArticleID], _read: models::Read, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_article_marked(&self, _articles: &[ArticleID], _marked: models::Marked, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_feed_read(&self, _feeds: &[FeedID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_category_read(&self, _categories: &[CategoryID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_tag_read(&self, _tags: &[TagID], _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn set_all_read(&self, _last_sync: DateTime<Utc>, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn add_feed(
        &self,
        url: &Url,
        title: Option<String>,
        category_id: Option<CategoryID>,
        client: &Client,
    ) -> FeedApiResult<(Feed, Option<Category>)> {
        let feed_request = client.get(url.as_str()).send().await.context(FeedApiErrorKind::Url)?;

        if !feed_request.status().is_success() {
            error!("Downloading feed failed: {}", url);
            return Err(FeedApiErrorKind::Url.into());
        }

        let result_bytes = feed_request
            .bytes()
            .await
            .map_err(|err| {
                error!("Reading response as bytes failed: {}", url);
                err
            })
            .context(FeedApiErrorKind::IO)?;

        if let Ok(feed) = parser::parse(result_bytes.as_ref()) {
            let title = match title {
                Some(title) => title,
                None => match feed.title {
                    Some(title) => title.content,
                    None => "Unknown Feed".to_owned(),
                },
            };

            let mut website: Option<Url> = None;
            if !feed.links.is_empty() {
                // see if there is a link with rel='alternate' -> this is probably what we want
                if let Some(link) = feed.links.iter().find(|link| link.rel == Some("alternate".to_owned())) {
                    if let Ok(url) = Url::parse(&link.href) {
                        website = Some(url);
                    }
                }
                // otherwise just take the first link
                else if let Ok(url) = Url::parse(&feed.links[0].href) {
                    website = Some(url);
                }
            }

            let icon_url = match feed.icon {
                Some(image) => match Url::parse(&image.uri) {
                    Ok(url) => Some(url),
                    Err(_) => None,
                },
                None => None,
            };

            // if the feed itself doesn't contain any useful way to generate an ID hash the url instead
            let url_string = url.to_string();
            let feed_id = if !feed.links.iter().any(|link| link.href == url_string) {
                let mut hasher = DefaultHasher::new();
                for byte in url_string.as_bytes() {
                    hasher.write_u8(*byte);
                }
                let url_hash = format!("{:x}", hasher.finish());
                FeedID::new(&url_hash)
            } else {
                FeedID::new(&feed.id)
            };

            let feed = Feed {
                feed_id,
                label: title,
                website,
                feed_url: Some(url.clone()),
                icon_url,
                sort_index: None,
            };

            let categories = self.portal.get_categories().context(FeedApiErrorKind::Portal)?;
            let category = categories.iter().find(|c| Some(&c.category_id) == category_id.as_ref()).cloned();

            return Ok((feed, category));
        }

        Err(FeedApiErrorKind::Unsupported.into())
    }

    async fn remove_feed(&self, _id: &FeedID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn move_feed(&self, _feed_id: &FeedID, _from: &CategoryID, _to: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_feed(&self, feed_id: &FeedID, _new_title: &str, _client: &Client) -> FeedApiResult<FeedID> {
        Ok(feed_id.clone())
    }

    async fn add_category(&self, title: &str, _parent: Option<&CategoryID>, _client: &Client) -> FeedApiResult<CategoryID> {
        Ok(CategoryID::new(title))
    }

    async fn remove_category(&self, _id: &CategoryID, _remove_children: bool, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_category(&self, id: &CategoryID, _new_title: &str, _client: &Client) -> FeedApiResult<CategoryID> {
        Ok(id.clone())
    }

    async fn move_category(&self, _id: &CategoryID, _parent: &CategoryID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn import_opml(&self, _opml: &str, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn add_tag(&self, title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Ok(TagID::new(title))
    }

    async fn remove_tag(&self, _id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn rename_tag(&self, id: &TagID, _new_title: &str, _client: &Client) -> FeedApiResult<TagID> {
        Ok(id.clone())
    }

    async fn tag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn untag_article(&self, _article_id: &ArticleID, _tag_id: &TagID, _client: &Client) -> FeedApiResult<()> {
        Ok(())
    }

    async fn get_favicon(&self, _feed_id: &FeedID, _client: &Client) -> FeedApiResult<FavIcon> {
        Err(FeedApiErrorKind::Unsupported.into())
    }
}
