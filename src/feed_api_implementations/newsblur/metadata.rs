use super::config::AccountConfig;
use super::NewsBlurService;
use crate::feed_api::{ApiMetadata, FeedApi, FeedApiErrorKind, FeedApiResult, Portal};
use crate::models::{LoginGUI, PasswordLoginGUI, PluginID, PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType, Url, VectorIcon};
use failure::{Fail, ResultExt};
use newsblur_api::NewsBlurApi;
use rust_embed::RustEmbed;
use std::path::PathBuf;
use std::str;

#[derive(RustEmbed)]
#[folder = "src/feed_api_implementations/newsblur/icons"]
struct NewsBlurResources;

pub struct NewsBlurMetadata;

impl NewsBlurMetadata {
    pub fn get_id() -> PluginID {
        PluginID::new("newsblur")
    }
}

impl ApiMetadata for NewsBlurMetadata {
    fn id(&self) -> PluginID {
        Self::get_id()
    }

    fn info(&self) -> FeedApiResult<PluginInfo> {
        let icon_data = NewsBlurResources::get("feed-service-newsblur.svg").ok_or(FeedApiErrorKind::Resource)?;
        let icon = VectorIcon {
            data: icon_data.to_vec(),
            width: 48,
            height: 48,
        };
        let icon = PluginIcon::Vector(icon);

        let symbolic_icon_data = NewsBlurResources::get("feed-service-newsblur-symbolic.svg").ok_or(FeedApiErrorKind::Resource)?;
        let symbolic_icon = VectorIcon {
            data: symbolic_icon_data.to_vec(),
            width: 48,
            height: 48,
        };
        let symbolic_icon = PluginIcon::Vector(symbolic_icon);

        let login_gui = LoginGUI::Password(PasswordLoginGUI { url: true, http_auth: false });

        Ok(PluginInfo {
            id: self.id(),
            name: String::from("NewsBlur"),
            icon: Some(icon),
            icon_symbolic: Some(symbolic_icon),
            website: Url::parse("https://newsblur.com").ok(),
            service_type: ServiceType::Remote { self_hosted: true },
            license_type: ServiceLicense::GPlv3,
            service_price: ServicePrice::Free,
            login_gui,
        })
    }

    fn parse_error(&self, error: &dyn Fail) -> Option<String> {
        Some(format!("{}", error))
    }

    fn get_instance(&self, path: &PathBuf, portal: Box<dyn Portal>) -> FeedApiResult<Box<dyn FeedApi>> {
        let account_config = AccountConfig::load(path).context(FeedApiErrorKind::Config)?;
        let mut api: Option<NewsBlurApi> = None;

        if let Some(url) = account_config.get_url() {
            if let Ok(url) = Url::parse(&url) {
                if let Some(username) = account_config.get_user_name() {
                    if let Some(password) = account_config.get_password() {
                        if let Some(cookie_string) = account_config.get_cookie_string() {
                            api = Some(NewsBlurApi::new(&url, &username, &password, Some(cookie_string)));
                        }
                    }
                }
            }
        }

        let logged_in = api.is_some();

        let newsblur = NewsBlurService {
            portal: portal,
            api,
            logged_in,
            config: account_config,
        };
        let newsblur = Box::new(newsblur);
        Ok(newsblur)
    }
}
