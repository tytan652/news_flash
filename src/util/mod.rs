pub mod favicon_cache;
pub mod feed_parser;
pub mod html2text;
pub mod opml;
pub mod text2html;

use libxml::{
    parser::{Parser, ParserOptions},
    tree::SaveOptions,
    xpath::Context,
};
use std::path::Path;

use crate::models::Url;

// Two goals here:
//   1) append an * after every term so it becomes a prefix search
//      (see <https://www.sqlite.org/fts3.html#section_3>), and
//   2) strip out common words/operators that might get interpreted as
//      search operators.
// We ignore everything inside quotes to give the user a way to
// override our algorithm here.  The idea is to offer one search query
// syntax for Geary that we can use locally and via IMAP, etc.
pub fn prepare_search_term(search_term: &str) -> String {
    let mut search_term_balanced = search_term.replace("'", " ");

    // Remove the last quote if it's not balanced.  This has the
    // benefit of showing decent results as you type a quoted phrase.
    if count_char(&search_term, &'"') % 2 != 0 {
        if let Some(last_quote) = search_term.rfind('"') {
            search_term_balanced.replace_range(last_quote..last_quote + 1, " ");
        }
    }

    let mut in_quote = false;
    let mut prepared_search_term = String::new();
    for word in search_term_balanced.split_whitespace() {
        let mut quotes = count_char(word, &'"');
        let mut word = word.to_owned();

        if !in_quote && quotes > 0 {
            in_quote = true;
            quotes -= 1;
        }

        if !in_quote {
            let lower = word.to_lowercase();
            if lower == "and" || lower == "or" || lower == "not" || lower == "near" {
                continue;
            }

            if word.starts_with('-') {
                word.remove(0);
            }

            if word == "" {
                continue;
            }

            word = format!("\"{}*\"", word);
        }

        if in_quote && quotes % 2 != 0 {
            in_quote = false;
        }

        prepared_search_term.push_str(&word);
        prepared_search_term.push_str(" ");
    }

    prepared_search_term
}

pub fn count_char(string: &str, character: &char) -> usize {
    string.chars().filter(|c| c == character).count()
}

pub fn vec_to_option<T>(vector: Vec<T>) -> Option<Vec<T>> {
    if vector.is_empty() {
        None
    } else {
        Some(vector)
    }
}

pub fn option_to_bool(option: Option<bool>) -> bool {
    match option {
        Some(value) => value,
        None => false,
    }
}

pub fn file_size(path: &Path) -> Result<u64, std::io::Error> {
    let meta_data = std::fs::metadata(path)?;
    if !meta_data.is_file() {
        return Err(std::io::ErrorKind::InvalidInput.into());
    }

    Ok(meta_data.len())
}

pub fn folder_size(path: &Path) -> Result<u64, std::io::Error> {
    let mut dir_size = 0;

    let dir = std::fs::read_dir(path)?;

    for file in dir {
        let file = file?;
        let metadata = file.metadata()?;
        let size = match metadata {
            metadata if metadata.is_dir() => folder_size(&file.path())?,
            metadata => metadata.len(),
        };

        dir_size += size;
    }

    Ok(dir_size)
}

pub fn complete_urls(html: &str, xml_base: &Url) -> Option<String> {
    let parser = Parser::default_html();
    let parser_options = ParserOptions {
        no_implied: true,
        no_def_dtd: true,
        ..Default::default()
    };
    if let Ok(doc) = parser.parse_string_with_options(html, parser_options) {
        if let Ok(xpath_ctx) = Context::new(&doc) {
            if let Ok(a_obj) = xpath_ctx.evaluate("//a") {
                let a_nodes = a_obj.get_nodes_as_vec();
                for mut a_node in a_nodes {
                    if let Some(href) = a_node.get_attribute("href") {
                        if let Err(url::ParseError::RelativeUrlWithoutBase) = url::Url::parse(&href) {
                            if let Ok(new_url) = xml_base.join(&href) {
                                let _res = a_node.set_attribute("href", new_url.as_str());
                            }
                        }
                    }
                }
            }

            if let Ok(img_obj) = xpath_ctx.evaluate("//img") {
                let img_nodes = img_obj.get_nodes_as_vec();
                for mut img_node in img_nodes {
                    if let Some(src) = img_node.get_attribute("src") {
                        if let Err(url::ParseError::RelativeUrlWithoutBase) = url::Url::parse(&src) {
                            if let Ok(new_url) = xml_base.join(&src) {
                                let _res = img_node.set_attribute("src", new_url.as_str());
                            }
                        }
                    }
                }
            }

            let options = SaveOptions {
                no_empty_tags: true,
                as_html: true,
                ..Default::default()
            };
            let html = doc.to_string_with_options(options);
            return Some(html);
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use crate::models::Url;

    #[test]
    fn complete_urls() {
        let html_fragment = r#"<figure><a href="tar-flac-subset-compress.svg"><img src="tar-flac-subset-compress.svg" alt="Compression results on incompressible data."/></a><figcaption><p>Compression results on incompressible data.</p></figcaption></figure>"#;
        let parsed_fragment = r#"<figure><a href="https://insanity.industries/post/pareto-optimal-compression/tar-flac-subset-compress.svg"><img src="https://insanity.industries/post/pareto-optimal-compression/tar-flac-subset-compress.svg" alt="Compression results on incompressible data."></a><figcaption><p>Compression results on incompressible data.</p></figcaption></figure>"#;
        let base_url = Url::parse("https://insanity.industries/post/pareto-optimal-compression/").unwrap();

        let res = super::complete_urls(html_fragment, &base_url).unwrap();
        assert_eq!(res.trim(), parsed_fragment);
    }
}
